#******************************************************************
#**
#**  s3cmd-conf master build script
#**
#******************************************************************
#** 
#** Copyright (C) 2012 Snowball Factory Inc.
#** 
#******************************************************************

# we need cmake 2.8.3
cmake_minimum_required(VERSION 2.8.3)

# setup our package info
SET(ANTIPASTI_NAME "s3cmd-conf")
SET(ANTIPASTI_ARCH_ALL 1)
SET(ANTIPASTI_DEPENDS "s3cmd, bash, globalconf")
SET(ANTIPASTI_CONTROL_FILES "conffiles")
SET(ANTIPASTI_CONTACT "Awe.sm Packages <packages@awe.sm>")
SET(ANTIPASTI_DESCRIPTION "s3cmd configuration
 Configuration files for s3cmd package.")

# setup our files to install
SET(CMAKE_INSTALL_PREFIX "/")
INSTALL(PROGRAMS
	s3cmd
	DESTINATION "usr/local/etc/globalconf/conf.d")
INSTALL(FILES
	s3cfg
	DESTINATION "usr/local/etc/globalconf/conf.skel/s3cmd")
INSTALL(FILES
	copyright
	DESTINATION "usr/local/share/doc/${ANTIPASTI_NAME}")
INSTALL(FILES
	placeholder
	DESTINATION "root"
	RENAME .s3cfg)

# include antipasti
INCLUDE(Antipasti)

